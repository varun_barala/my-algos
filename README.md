# Algorithms
-------------
     ## Graph Theory [AdjacencyLists are used as DS]
         * BFS [Package `com.algo.concepts.graphs.bfs`]
             * Shortest path in unweighted graph
             * Connected Components
             * BiPartite
         * DFS [Package `com.algo.concepts.graphs.dfs`]
             * TopologicalSorting
             * Detect cycle
         * Minimum spanning tree
             * Prim's algo
             * Kruskal's algo
         * Shortest distance
             * Dijkstra algo

# Data Structures
------------------
