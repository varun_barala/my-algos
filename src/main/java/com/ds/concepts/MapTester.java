package com.ds.concepts;

/**
 * 
 * @author ooo
 *
 */
public class MapTester {
    public static void main(String[] args) {
        Map<Integer, String> map = new Map<>(3);
        map.put(1, "1");
        System.out.println(map.get(1));
        map.put(2, "2");
        System.out.println(map.get(2));
        map.put(3, "3");
        System.out.println(map.get(3));
        map.put(3, "4");
        System.out.println(map.get(3));

        System.out.println(map.get(4));

        Map<Key, String> map1 = new Map<>(3);
        map1.put(new Key(1), "1");
        System.out.println(map1.get(new Key(1)));
        map1.put(new Key(2), "2");
        System.out.println(map1.get(new Key(2)));
        map1.put(new Key(3), "3");
        System.out.println(map1.get(new Key(3)));
        map1.put(new Key(3), "4");
        System.out.println(map1.get(new Key(3)));

        System.out.println(map.get(4));
    }

    private static class Key{
        int k;
        public Key(int key) {
            this.k = key;
        }

        @Override
        public int hashCode() {
            return 1;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj == null || !(obj instanceof Key)) return false;
            if(this == obj) return true;
            Key that = (Key) obj;
            return this.k == that.k;
        }
    }
}