package com.ds.concepts;

/**
 * 
 * @author ooo
 *
 */
public interface IStack {
    public void push(int data);
    public int pop();
    public int top();
    public int size();
    public boolean isEmpty();
}
