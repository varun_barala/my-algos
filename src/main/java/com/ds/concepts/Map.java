package com.ds.concepts;

/**
 * 
 * @author ooo
 *
 */
public class Map<K,V> {
    private int size = 16;
    private final Bucket<K,V>[] buckets;

    public Map(int size) {
        this.size = size;
        this.buckets = new Bucket[size];
    }

    private static class Bucket<K,V>{
        K k;
        V v;
        Bucket<K,V> next;

        public Bucket(K k, V v) {
            this.v = v;
            this.k = k;
        }
    }

    public void put(K k, V v){
        int hash = k.hashCode()%size;
        Bucket<K,V> bucket = this.buckets[hash];

        //no collision
        if(bucket == null){
            this.buckets[hash] = new Bucket<>(k,v);
        }else{
            /*
             * 1) key already exists and this is update
             * 2) this is new key
             */
            while(bucket!=null){
                if(bucket.k.equals(k)){
                    bucket.v = v;
                    break;
                }
                bucket = bucket.next;
            }

            //new key
            if(bucket==null){
                bucket = new Bucket<>(k, v);
                bucket.next = this.buckets[hash];
                this.buckets[hash] = bucket;
            }
        }

        //check for resizing
    }

    public V get(K k){
        int hash = k.hashCode()%size;
        Bucket<K,V> bucket = this.buckets[hash];

        while(bucket!=null){
            if(bucket.k.equals(k)){
                return bucket.v;
            }
            bucket = bucket.next;
        }
        return null;
    }

    public boolean contains(K k){
        return (get(k)!=null);
    }
}