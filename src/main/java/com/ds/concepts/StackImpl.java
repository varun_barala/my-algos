package com.ds.concepts;

/**
 * Java stack extends vector and both are thread safe. And also part of collection framework
 * 
 * @author ooo
 *
 */
public class StackImpl implements IStack{

    private static int size = 16;
    private int pointer=0;
    private final int[] arr;

    public StackImpl() {
        this(size);
    }

    public StackImpl(int s) {
        size = s;
        this.arr = new int[size];
    }

    @Override
    public void push(int data) {
        if(pointer==size){
            throw new IllegalArgumentException("Stack is full");
        }

        this.arr[pointer++] = data;
    }

    @Override
    public int pop() {
        if(isEmpty()){
            throw new IllegalArgumentException("Stack is empty!");
        }

        return this.arr[--pointer];
    }

    @Override
    public int top() {
        if(isEmpty()){
            throw new IllegalArgumentException("Stack is empty!");
        }
        return this.arr[pointer-1];
    }

    @Override
    public int size() {
        return this.pointer;
    }

    @Override
    public boolean isEmpty() {
        return (this.pointer==0);
    }
}