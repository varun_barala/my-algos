package com.ds.concepts;

/**
 * 
 * @author ooo
 *
 */
public class StackTester {
    public static void main(String[] args) {
        StackImpl stackImpl = new StackImpl(3);
        System.out.println("is empty :: " +  stackImpl.isEmpty());

        stackImpl.push(0);
        System.out.println("top element :: " + stackImpl.top());
        System.out.println("size :: " + stackImpl.size());
        System.out.println("is empty :: " +  stackImpl.isEmpty());
        System.out.println("--------------------------");

        stackImpl.push(1);
        System.out.println("top element :: " + stackImpl.top());
        System.out.println("size :: " + stackImpl.size());
        System.out.println("is empty :: " +  stackImpl.isEmpty());
        System.out.println("--------------------------");

        stackImpl.push(2);
        System.out.println("top element :: " + stackImpl.top());
        System.out.println("size :: " + stackImpl.size());
        System.out.println("is empty :: " +  stackImpl.isEmpty());
        System.out.println("--------------------------");

        System.out.println(stackImpl.pop());
        System.out.println("top element :: " + stackImpl.top());
        System.out.println("size :: " + stackImpl.size());
        System.out.println("is empty :: " +  stackImpl.isEmpty());
        System.out.println("--------------------------");

        System.out.println(stackImpl.pop());
        System.out.println("top element :: " + stackImpl.top());
        System.out.println("size :: " + stackImpl.size());
        System.out.println("is empty :: " +  stackImpl.isEmpty());
        System.out.println("--------------------------");

        System.out.println(stackImpl.pop());
        System.out.println("size :: " + stackImpl.size());
        System.out.println("is empty :: " +  stackImpl.isEmpty());
        System.out.println("--------------------------");
    }
}
