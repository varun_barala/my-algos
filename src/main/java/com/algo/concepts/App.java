package com.algo.concepts;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws InterruptedException
    {
        while(true){
            System.out.println( "Hello World!" );
            Thread.sleep(3000);
        }
    }
}
