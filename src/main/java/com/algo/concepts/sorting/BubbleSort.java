package com.algo.concepts.sorting;

import java.util.Arrays;

/**
 * 
 * @author ooo
 *
 */
public class BubbleSort {
    public static void main(String[] args) {
        int [] arr = {4,5,2,1,3,4,-1,3,4};
        bubbleSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void bubbleSort(int[] arr){
        int n = arr.length;
        for(int i=0;i<n-1;i++){
            for(int j=0;j<n-i-1;j++){
                if(arr[j+1]<arr[j]){
                    int tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                }
            }
        }
    }
}