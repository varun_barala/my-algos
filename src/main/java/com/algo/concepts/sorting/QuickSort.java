package com.algo.concepts.sorting;

import java.util.Arrays;

/**
 * 
 * @author ooo
 *
 */
public class QuickSort {

    private int partition(int[] arr, int l, int h){
        int pIndex = l-1;
        int pivot = arr[h];

        for(int i=l;i<h;i++){
            if(arr[i] <= pivot){
                pIndex++;
                int temp = arr[i];
                arr[i] = arr[pIndex];
                arr[pIndex] = temp;
            }
        }

        int temp = arr[h];
        arr[h] = arr[++pIndex];
        arr[pIndex] = temp;

        return pIndex;
    }

    public void quickSort(int[] arr, int l, int h) {
        if(l<h){
            int pIndex = partition(arr, l, h);
            quickSort(arr, l, pIndex-1);
            quickSort(arr, pIndex+1, h);
        }
    }

    public static void main(String[] args) {
        int [] arr = {4,5,2,1,3,4,-1,3,4};
        QuickSort quickSort = new QuickSort();
        quickSort.quickSort(arr, 0, arr.length-1);
        System.out.println(Arrays.toString(arr));
    }
}