package com.algo.concepts.sorting;

import java.util.Arrays;

/**
 * 
 * @author ooo
 *
 */
public class MergeSort {

    private void mergeArrays(int[] arr, int l, int m ,int h){
        int n1 = m-l+1;
        int n2 = h-m;
        int[] L = new int[n1];
        int[] R = new int[h-m];

        for(int i=0;i<n1;i++){
            L[i] = arr[i+l];
        }

        for(int i=0;i<n2;i++){
            R[i] = arr[m+1+i];
        }

        int i=0,j=0,k=l;
        while(i<n1 && j<n2 && k<=h){
            if(L[i]<=R[j]){
                arr[k++] = L[i++];
            }else{
                arr[k++] = R[j++];
            }
        }

        while(i<n1 && k<=h){
            arr[k++] = L[i++];
        }

        while(j<n2 && k<=h){
            arr[k++] = R[j++];
        }
    }

    public void mergeSort(int[] arr, int l, int h){
        if(l<h){
            int m = (l+h)/2;
            mergeSort(arr, l, m);
            mergeSort(arr, m+1, h);
            mergeArrays(arr, l, m, h);
        }
    }

    public static void main(String[] args) {
        int [] arr = {4,5,2,1,3,4,-1,3,4};
        MergeSort mergeSort = new MergeSort();
        mergeSort.mergeSort(arr, 0, arr.length-1);
        System.out.println(Arrays.toString(arr));
    }
}
