package com.algo.concepts.graphs.factory;

import com.algo.concepts.graphs.Edge;
import com.algo.concepts.graphs.WeightedGraph;
import com.algo.concepts.graphs.WeightedGraphImpl;

/**
 * 
 * @author ooo
 *
 */
public class WeightedGraphFactory {

    /*
     *  0----(w 2)--------1___
     *  | \               |   \
     *  |   \             |    \
     *  |     \           |     \
     *  |       \ (w 4)   |(w 1) \ 
     *  |         \       |       \(w 3)
     *  |           \     |        \
     *  |(w 1)        \   |         \
     *  2------(w 3)-----3----(w 1)--4
     */
    public static WeightedGraph weightedGraph1(){
        WeightedGraph weightedGraph = new WeightedGraphImpl(5, false);
        weightedGraph.addEdge(new Edge(0, 1, 2));
        weightedGraph.addEdge(new Edge(0, 2, 1));
        weightedGraph.addEdge(new Edge(0, 3, 4));
        weightedGraph.addEdge(new Edge(1, 3, 1));
        weightedGraph.addEdge(new Edge(1, 4, 3));
        weightedGraph.addEdge(new Edge(2, 3, 3));
        weightedGraph.addEdge(new Edge(3, 4, 1));
        return weightedGraph;
    }
}
