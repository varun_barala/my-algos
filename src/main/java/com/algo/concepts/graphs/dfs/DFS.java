package com.algo.concepts.graphs.dfs;

import java.util.Arrays;
import java.util.BitSet;

import com.algo.concepts.graphs.Graph;

/**
 * Important Notes:
 * ----------------
 * 1) `entryTime` and `exitTime` helps to answer ancestor and descendants questions
 * 2) DFS tree will have back and tree edges
 *      back edge [descendant to ancestor]
 *      tree edge [towards new vertex]
 * 3) DFS is same as BFS if we use stack instead of queue in the BFS implementation
 * 
 * @author ooo
 *
 */
public abstract class DFS {
    protected final Graph g;
    protected final BitSet discovered;
    protected final BitSet processed;
    protected final int[] entryTime;
    protected final int[] exitTime;
    protected final int[] parent;
    private int time;

    public DFS(Graph g) {
        this.g = g;
        this.discovered = new BitSet(this.g.size());
        this.processed = new BitSet(this.g.size());
        this.entryTime = new int[this.g.size()];
        this.exitTime = new int[this.g.size()];
        this.parent = new int[this.g.size()];
        this.time = 0;
    }

    private void dfsUtil(int src){
        this.entryTime[src] = ++time;
        this.discovered.set(src);

        processVertexEarlier(src);

        for(Integer edge : g.getAdjacentVertices(src)){
            if(!this.discovered.get(edge)){
                this.parent[edge] = src;
                processEdge(src, edge);
                dfsUtil(edge);
            } else if(shallProcess(src, edge)){
                processEdge(src, edge);
            }
        }

        processVertexLater(src);

        this.exitTime[src] = ++time;
        this.processed.set(src);
    }

    private boolean shallProcess(int src, Integer edge) {
        //TODO implement later
        return false;
    }

    public void dfs(int src){
        clearMetadata();
        dfsUtil(src);
    }

    private void clearMetadata() {
        this.discovered.clear();
        Arrays.fill(this.entryTime, -1);
        Arrays.fill(this.exitTime, -1);
        Arrays.fill(this.parent, -1);
        this.time = 0;
    }

    abstract void processVertexEarlier(int src);
    abstract void processVertexLater(int src);
    abstract void processEdge(int src, int dest);
}