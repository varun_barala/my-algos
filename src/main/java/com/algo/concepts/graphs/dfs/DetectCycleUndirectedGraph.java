package com.algo.concepts.graphs.dfs;

import com.algo.concepts.graphs.Graph;

/**
 * 
 * @author ooo
 *
 */
public class DetectCycleUndirectedGraph extends DFS{

    public DetectCycleUndirectedGraph(Graph g) {
        super(g);
    }

    @Override
    void processVertexEarlier(int src) {
    }

    @Override
    void processVertexLater(int src) {
    }

    @Override
    void processEdge(int src, int dest) {
        /*
         * tree edge:- `src` will be the parent of the `dest`
         * back edge:- `dest` will have different parent
         */
        if(super.parent[dest] != src){
            System.out.println("found duplicate");
        }
    }
}