package com.algo.concepts.graphs.dfs;

import java.util.List;
import java.util.Stack;
import com.algo.concepts.graphs.Graph;

/**
 * 
 * @author ooo
 *
 */
public class DAGTopologicalSorting extends DFS{
    private final Stack<Integer> SORTED_ORDER;
    private static final String NOT_DAG = "Given graph is not DAG!";

    public DAGTopologicalSorting(Graph g) {
        super(g);
        if(!g.isDirected()){
            throw new IllegalArgumentException("Only applicable for DAG!");
        }
        this.SORTED_ORDER = new Stack<>();
    }

    @Override
    void processVertexEarlier(int src) {
    }

    @Override
    void processVertexLater(int src) {
        this.SORTED_ORDER.push(src);
    }

    @Override
    void processEdge(int src, int dest) {
        //TODO fix it for directed graph
        if(this.parent[dest]!=src){
            throw new IllegalArgumentException(NOT_DAG);
        }
    }

    public Stack<Integer> topologicalSorting(){
        List<Integer> vertices = super.g.getAllVertices();
        for(Integer vertex : vertices){
            if(!super.discovered.get(vertex)){
                dfs(vertex);
            }
        }
        return this.SORTED_ORDER;
    }
}