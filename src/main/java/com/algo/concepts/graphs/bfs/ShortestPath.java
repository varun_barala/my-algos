package com.algo.concepts.graphs.bfs;

import java.util.Arrays;

import com.algo.concepts.graphs.Graph;
import com.algo.concepts.graphs.factory.GraphFactory;
import com.sun.scenario.effect.impl.prism.PrImage;

public class ShortestPath extends BFS{

    public ShortestPath(Graph graph) {
        super(graph);
    }

    @Override
    protected void processVertexEarlier(int src) {
        // TODO Auto-generated method stub
    }

    @Override
    protected void processVertexLater(int src) {
        // TODO Auto-generated method stub
    }

    @Override
    protected void processEdge(int src, int dest) {
        // TODO Auto-generated method stub
    }

    public void findPath(int src, int dest){
        super.bfs(src);
        findPathUtil(src, dest);
    }

    private void findPathUtil(int src, int dest){
        if(src == dest || dest == -1){
            System.out.print(" " + src);
            return;
        }

        findPathUtil(src, super.parent[dest]);
        System.out.print(" -> " + dest);
    }

    private void print(){
        System.out.println();
        System.out.println(Arrays.toString(super.parent));
    }

    public static void main(String[] args) {
        final Graph g = GraphFactory.graph5(false);
        ShortestPath fPath = new ShortestPath(g);
        fPath.findPath(0, 6);
        //assertion 0 -> 1 -> 4 -> 6
        fPath.print();
    }
}