package com.algo.concepts.graphs.bfs;

import java.util.Arrays;

import com.algo.concepts.graphs.Graph;

/**
 *
 * @author ooo
 *
 */
public class BiPartite extends BFS{
    private final COLOR[] colors;
    private boolean isBiPartite = true;

    public BiPartite(Graph graph) {
        super(graph);
        this.colors = new COLOR[graph.size()];
        Arrays.fill(colors, COLOR.NOT_COLORED);
    }

    private static enum COLOR{
        BLACK,
        WHITE,
        NOT_COLORED;
    }

    @Override
    protected void processVertexEarlier(int src) {
    }

    @Override
    protected void processVertexLater(int src) {
    }

    @Override
    protected void processEdge(int src, int dest) {
        if(colors[src] == colors[dest]){
            
            this.isBiPartite = false;
            System.out.println("Graph is not Bipartite!, [" + src + ", " + dest + "]");
        }

        colors[dest] = complimentColor(colors[src]);
    }

    private COLOR complimentColor(COLOR color){
        switch (color) {
        case BLACK:
            return COLOR.WHITE;
        case WHITE:
            return COLOR.BLACK;
        default:
            return COLOR.NOT_COLORED;
        }
    }

    public static void main(String[] args) {
    }
}