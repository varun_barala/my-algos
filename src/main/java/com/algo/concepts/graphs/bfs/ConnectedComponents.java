package com.algo.concepts.graphs.bfs;

import java.util.List;

import com.algo.concepts.graphs.Graph;

/**
 * Number of connected components are same as number of times performed BFS to make every vertex discovered in given graph
 * 
 * @author ooo
 *
 */
public class ConnectedComponents extends BFS{

    public ConnectedComponents(Graph graph) {
        super(graph);
    }

    public int countConnectedComponents(){
        List<Integer> vertices = super.g.getAllVertices();
        int count = 0;
        for(Integer vertex : vertices){
            if(!super.discovered.get(vertex)){
                count++;
                super.bfs(vertex);
            }
        }
        return count;
    }

    @Override
    protected void processVertexEarlier(int src) {
    }

    @Override
    protected void processVertexLater(int src) {
    }

    @Override
    protected void processEdge(int src, int dest) {
    }
}