package com.algo.concepts.graphs.bfs;

import java.util.Arrays;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.algo.concepts.graphs.Graph;

/**
 * 
 * O(V+E) adjacency list
 * O(V*V) adjacency matrix
 * 
 * @author ooo
 *
 */
public abstract class BFS {
    protected final Graph g;
    protected final BitSet discovered;
    protected final BitSet processed;
    protected final int[] parent; //maintains the parent child links

    public BFS(final Graph graph) {
        this.g = graph;
        this.discovered = new BitSet(this.g.size());
        this.processed = new BitSet(this.g.size());
        this.parent = new int[this.g.size()];
    }

    private void clearMetadata(){
        this.discovered.clear();
        this.processed.clear();
        Arrays.fill(this.parent, -1);
    }

    public void bfs(int src){
        clearMetadata();

        Queue<Integer> queue = new LinkedList<>();
        queue.add(src);
        this.discovered.set(src);

        while(!queue.isEmpty()){
            src = queue.poll();
            this.processed.set(src);

            processVertexEarlier(src);

            for(Integer edge : g.getAdjacentVertices(src)){
                if(!this.discovered.get(edge) || this.g.isDirected()){
                    processEdge(src, edge);
                }

                if(!this.discovered.get(edge)){
                    this.discovered.set(edge);
                    queue.add(edge);
                    this.parent[edge] = src;
                }
            }

            processVertexLater(src);
        }
    }

    /*
     * These abstract methods are very useful to solve other problems
     */
    protected abstract void processVertexEarlier(int src);
    protected abstract void processVertexLater(int src);
    protected abstract void processEdge(int src, int dest);
}