package com.algo.concepts.graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Can be merged with {@link AdjacencyListGraphImpl} If we make generic.
 * For time being this(^) thought > /dev/null
 * 
 * @author ooo
 *
 */
public class WeightedGraphImpl implements WeightedGraph{
    private final List<List<Edge>> graph;
    private final List<Edge> allEdges;
    private final int size;
    private final boolean isDirected;

    public WeightedGraphImpl(int size, boolean isDirected) {
        this.size = size;
        this.isDirected = isDirected;
        this.allEdges = new ArrayList<>();
        this.graph = new ArrayList<>();
        for(int i=0;i<size;i++){
            this.graph.add(new ArrayList<>());
        }
    }

    @Override
    public void addEdge(Edge edge) {
        this.allEdges.add(edge);
        this.graph.get(edge.source).add(edge);
        if(!isDirected){
            this.graph.get(edge.destination).add(edge);
        }
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public List<Integer> getAllVertices() {
        return IntStream.range(0, size)
                .boxed()
                .collect(Collectors.toList());
    }

    @Override
    public boolean isDirected() {
        return this.isDirected;
    }

    @Override
    public List<Edge> getAdjacentEdges(int src) {
        return this.graph.get(src);
    }

    @Override
    public List<Edge> getAllEdges() {
        return this.allEdges;
    }
}