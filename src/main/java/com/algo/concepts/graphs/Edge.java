package com.algo.concepts.graphs;

public class Edge {
    public final int source;
    public final int destination;
    public final int weight;

    public Edge(int src, int dest, int weight) {
        this.source = src;
        this.destination = dest;
        this.weight = weight;
    }
}
