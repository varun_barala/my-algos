package com.algo.concepts.graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 
 * @author ooo
 *
 */
public class AdjacencyListGraphImpl implements Graph{
    private final boolean isDirected;
    private final int size;
    private final List<List<Integer>> adjacencyList;

    public AdjacencyListGraphImpl(boolean isDirected, int size) {
        this.isDirected = isDirected;
        this.size = size;
        this.adjacencyList = new ArrayList<>(size);
        for(int i=0;i<size;i++){
            this.adjacencyList.add(new ArrayList<>());
        }
    }

    @Override
    public boolean isDirected() {
        return this.isDirected;
    }

    @Override
    public List<Integer> getAdjacentVertices(int src) {
        return this.adjacencyList.get(src);
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public List<Integer> getAllVertices() {
        return IntStream.range(0, size)
                .boxed()
                .collect(Collectors.toList());
    }

    @Override
    public Graph transpose() {
        /*
         * transpose of undirected graph will remain same
         */

        return null;
    }

    @Override
    public void addEdge(int src, int dest) {
        this.adjacencyList.get(src).add(dest);
        if(!isDirected){
            this.adjacencyList.get(dest).add(src);
        }
    }
}