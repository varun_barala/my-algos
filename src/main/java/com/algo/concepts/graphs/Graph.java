package com.algo.concepts.graphs;

import java.util.List;

/**
 * 
 * @author ooo
 *
 */
public interface Graph {

    /**
     * Graph types whether it's directed graph or undirected
     * 
     * @return
     */
    public boolean isDirected();

    public List<Integer> getAdjacentVertices(int src);

    public int size();

    public List<Integer> getAllVertices();

    public Graph transpose();

    public void addEdge(final int src, final int dest);
}