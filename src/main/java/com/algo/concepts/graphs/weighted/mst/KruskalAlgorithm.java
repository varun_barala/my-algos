package com.algo.concepts.graphs.weighted.mst;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.algo.concepts.graphs.Edge;
import com.algo.concepts.graphs.WeightedGraph;

/**
 * 
 * @author ooo
 *
 */
public class KruskalAlgorithm {
    private final WeightedGraph graph;
    private static final Comparator<Edge> edgeComparator = (a, b) -> {return (a.weight-b.weight);};

    public KruskalAlgorithm(final WeightedGraph graph) {
        this.graph = graph;
    }

    public List<Edge> mst(){
        List<Edge> allEdges = this.graph.getAllEdges();
        Collections.sort(allEdges, edgeComparator);
        DisjointSet disjointSet = new DisjointSet();
        List<Edge> resultEdge = new ArrayList<>();

        for(Integer vertex : this.graph.getAllVertices()){
            disjointSet.add(vertex);
        }

        for(Edge edge : allEdges){
            long root1 = disjointSet.findSet(edge.source);
            long root2 = disjointSet.findSet(edge.destination);

            //if both are same that means edge will contribute to cycle
            if(root1 != root2){
                resultEdge.add(edge);
                disjointSet.union(edge.source, edge.destination);
            }
        }
        return resultEdge;
    }
}