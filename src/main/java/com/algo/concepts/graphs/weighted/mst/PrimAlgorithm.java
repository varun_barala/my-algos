package com.algo.concepts.graphs.weighted.mst;

import java.util.Arrays;
import java.util.BitSet;
import java.util.PriorityQueue;

import com.algo.concepts.graphs.Edge;
import com.algo.concepts.graphs.WeightedGraph;
import com.algo.concepts.graphs.factory.WeightedGraphFactory;

/**
 * 
 * @author ooo
 *
 */
public class PrimAlgorithm {
    private final BitSet addedToMST;
    private final WeightedGraph graph;
    private final int[] value;
    private final int[] parent;

    public PrimAlgorithm(final WeightedGraph graph) {
        this.graph = graph;
        this.addedToMST = new BitSet(graph.size());
        this.value = new int[graph.size()];
        this.parent = new int[graph.size()];
        Arrays.fill(value, Integer.MAX_VALUE);
        Arrays.fill(parent, -1);
    }

    private void clearMetadata(){
        addedToMST.clear();
        Arrays.fill(value, Integer.MAX_VALUE);
        Arrays.fill(parent, -1);
    }

    public void mst(int source){
        clearMetadata();
        value[source] = -1; //starting point will have -1 value

        PriorityQueue<NodeWeight> queue = new PriorityQueue<>();
        queue.add(new NodeWeight(source, -1));

        while(!queue.isEmpty()){
            source = queue.poll().nodeId;
            addedToMST.set(source);
            
            for(Edge edge : this.graph.getAdjacentEdges(source)){
                if(!addedToMST.get(edge.destination)){
                    if(value[edge.destination] > edge.weight){
                        this.value[edge.destination] = edge.weight;
                        queue.add(new NodeWeight(edge.destination, edge.weight));
                        this.parent[edge.destination] = source;
                    }
                }
            }
        }
    }

    private void printMst(){
        for(Integer vertex : this.graph.getAllVertices()){
            System.out.println("vertex : " + vertex +
                    ", parent : " + (parent[vertex]==-1?vertex:parent[vertex]) +
                    ", weight : " + (value[vertex]==-1?0:value[vertex]));
        }
    }

    private static class NodeWeight implements Comparable<NodeWeight>{
        int nodeId;
        int weight;

        public NodeWeight(int nodeId, int weight) {
            this.nodeId = nodeId;
            this.weight = weight;
        }

        @Override
        public int compareTo(NodeWeight o) {
            return this.weight-o.weight;
        }
    }

    public static void main(String[] args) {
        final WeightedGraph graph = WeightedGraphFactory.weightedGraph1();
        PrimAlgorithm primAlgorithm = new PrimAlgorithm(graph);

        primAlgorithm.mst(0);
        primAlgorithm.printMst();
        /*
         * expected result:
         * 
         * (0) 2-> (1) 1-> (3) 1-> (4)
         * (0) 1-> (2)
         * 
         */
    }
}