package com.algo.concepts.graphs.weighted.mst;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author ooo
 *
 */
public class DisjointSet implements IDisjointSet{
    private final Map<Long, Unode> mapping;

    public DisjointSet() {
        this.mapping = new HashMap<>();
    }

    @Override
    public void add(long data) {
        Unode node = new Unode();
        node.rank = 0;
        node.data = data;
        node.parent = node;
        this.mapping.put(data, node);
    }

    @Override
    public boolean union(long data1, long data2) {
        Unode node1 = this.mapping.get(data1);
        Unode node2 = this.mapping.get(data2);

        if(node1 == null || node2 == null){
            return false;
        }

        Unode parent1 = findSet(node1);
        Unode parent2 = findSet(node2);

        if(parent1.data == parent2.data){
            return false;
        }

        if(parent1.rank >= parent2.rank){
            parent1.rank = (parent1.rank == parent2.rank)?(parent1.rank+1):(parent1.rank);
            parent2.parent = parent1;
        }else{
            parent1.parent = parent2;
        }

        return true;
    }

    @Override
    public long findSet(long data) {
        Unode node = this.mapping.get(data);
        if(node == null){
            return -1L;
        }
        return findSet(node).data;
    }

    private Unode findSet(Unode node){
        if(node.parent == node){
            return node.parent;
        }

        //path compression
        node.parent = findSet(node.parent);
        return node.parent;
    }

    private static class Unode{
        int rank;
        Unode parent;
        long data;
    }

    public static void main(String args[]) {
        DisjointSet ds = new DisjointSet();
        ds.add(1);
        ds.add(2);
        ds.add(3);
        ds.add(4);
        ds.add(5);
        ds.add(6);
        ds.add(7);

        ds.union(1, 2);
        ds.union(2, 3);
        ds.union(4, 5);
        ds.union(6, 7);
        ds.union(5, 6);
        ds.union(3, 7);

        /*
         * expected parent `4` for all
         */
        System.out.println(ds.findSet(1));
        System.out.println(ds.findSet(2));
        System.out.println(ds.findSet(3));
        System.out.println(ds.findSet(4));
        System.out.println(ds.findSet(5));
        System.out.println(ds.findSet(6));
        System.out.println(ds.findSet(7));
    }
}