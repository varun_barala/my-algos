package com.algo.concepts.graphs.weighted.shortestpaths;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Comparator;
import java.util.PriorityQueue;

import com.algo.concepts.graphs.Edge;
import com.algo.concepts.graphs.WeightedGraph;
import com.algo.concepts.graphs.WeightedGraphImpl;

/**
 *
 * @author ooo
 *
 */
public class DijkstraAlgorithm {
    private final WeightedGraph graph;
    private final BitSet visited;
    private final BitSet addedToQueue;
    private final int[] parent;
    private final int[] distance;
    private static final Comparator<Edge> edgeComparator = (a, b) -> {return (a.weight-b.weight);};

    public DijkstraAlgorithm(final WeightedGraph graph) {
        this.graph = graph;
        this.visited = new BitSet(graph.size());
        this.addedToQueue = new BitSet(graph.size());
        this.parent = new int[graph.size()];
        Arrays.fill(parent, -1);
        this.distance = new int[graph.size()];
        Arrays.fill(distance, Integer.MAX_VALUE);
    }

    private void clearMetadata(){
        this.visited.clear();
        this.addedToQueue.clear();
        Arrays.fill(parent, -1);
        Arrays.fill(distance, Integer.MAX_VALUE);
    }

    public int[] shortestDistance(int src){
        clearMetadata();
        distance[src] = 0;
        PriorityQueue<NodeWeight> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(new NodeWeight(src, 0));
        addedToQueue.set(src);
        while(!priorityQueue.isEmpty()){
            src = priorityQueue.poll().nodeId;
            visited.set(src);
            for(Edge edge : this.graph.getAdjacentEdges(src)){
                if(!visited.get(edge.destination)){
                    if(distance[src] + edge.weight < distance[edge.destination]){
                        this.parent[edge.destination] = src;
                        this.distance[edge.destination] = distance[src] + edge.weight;
                    }
                    if(!addedToQueue.get(edge.destination)){
                        priorityQueue.add(new NodeWeight(edge.destination, edge.weight));
                        addedToQueue.set(edge.destination);
                    }
                }
            }
        }
        return this.distance;
    }

    public static void main(String[] args) {
        final WeightedGraph graph = new WeightedGraphImpl(7, false);

        /*
         *   1   (5) 2
         *   |3       \ 2
         *   |    (3)  \
         *   5   4------3
         *   \   /
         * 2  | / 2
         *    6
         */
        graph.addEdge(new Edge(1, 2, 5));
        graph.addEdge(new Edge(2, 3, 2));
        graph.addEdge(new Edge(1, 4, 9));
        graph.addEdge(new Edge(1, 5, 3));
        graph.addEdge(new Edge(5, 6, 2));
        graph.addEdge(new Edge(6, 4, 2));
        graph.addEdge(new Edge(3, 4, 3));

        DijkstraAlgorithm dijkstraAlgorithm = new DijkstraAlgorithm(graph);
        int[] distance = dijkstraAlgorithm.shortestDistance(1);
        System.out.println(Arrays.toString(distance));
        /*
         *
         * 1 <--> 2  5
         * 1 <--> 5  3
         * 1 <--> 6  5
         * 1 <--> 4  7
         * 1 <--> 3  7
         */
    }

    private static class NodeWeight implements Comparable<NodeWeight>{
        int nodeId;
        int weight;

        public NodeWeight(int nodeId, int weight) {
            this.nodeId = nodeId;
            this.weight = weight;
        }

        @Override
        public int compareTo(NodeWeight o) {
            return this.weight-o.weight;
        }
    }
}