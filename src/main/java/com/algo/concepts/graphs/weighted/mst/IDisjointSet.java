package com.algo.concepts.graphs.weighted.mst;

/**
 * 
 * @author ooo
 * 
 *
 */
public interface IDisjointSet {
    public void add(long data);
    public boolean union(long data1, long data2);
    public long findSet(long data);
}