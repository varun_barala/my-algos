package com.algo.concepts.graphs;

import java.util.List;

/**
 * 
 * @author ooo
 *
 */
public interface WeightedGraph {
    public void addEdge(Edge edge);
    public int size();
    public List<Integer> getAllVertices();
    public List<Edge> getAdjacentEdges(int src);
    public boolean isDirected();
    public List<Edge> getAllEdges();
}
