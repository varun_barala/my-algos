package com.algo.concepts.linkedlist;

/**
 * 
 * @author ooo
 *
 */
public class LinkedListUtil {
    public static void print(LNode head){
        if(head == null){
            return;
        }
        while(head!=null){
            System.out.print("->(" + head.data+")");
            head = head.next;
        }
        System.out.println();
    }
}