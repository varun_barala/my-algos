package com.algo.concepts.linkedlist;

/**
 * 
 * @author ooo
 *
 */
public class ReverseLinkedList {

    public LNode reverse(LNode head){
        LNode prev=null, next, current=head;

        while(current!=null){
            next = current.next;
            current.next = prev;
            prev = current;
            current=next;
        }
        return prev;
    }

    public static void main(String[] args) {
        LNode head = new LNode(4);
        head.next = new LNode(5);
        head.next.next = new LNode(2);
        head.next.next.next = new LNode(1);

        ReverseLinkedList reverseLinkedList = new ReverseLinkedList();
        LinkedListUtil.print(head);
        LinkedListUtil.print(reverseLinkedList.reverse(head));
    }

    private static LNode re(LNode head){
        LNode prev=null, next;
        while(head!=null){
            next = head.next;
            head.next = prev;
            prev = head;
            head = next;
        }

        return prev;
    }
}
