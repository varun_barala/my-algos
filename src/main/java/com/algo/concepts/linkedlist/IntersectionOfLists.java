package com.algo.concepts.linkedlist;

public class IntersectionOfLists {

    private int length(LNode head){
        int count = 0;
        while(head!=null){
            count++;
            head = head.next;
        }
        return count;
    }

    public LNode intersectionPoint(LNode head1, LNode head2){
        int length1 = length(head1);
        int length2 = length(head2);

        if(length1 == length2){
            return head1;
        }

        int diff = 0;
        if(length1 > length2){
            diff = length1-length2;
            rewind(head1, diff);
        }else{
            diff = length2-length1;
            rewind(head2, diff);
        }

        while(head1!=null && head2!=null){
            if(head1.data == head2.data){
                return head1;
            }
            head1 = head1.next;
            head2 = head2.next;
        }

        //no intersection point exists
        return null;
    }

    private void rewind(LNode head, int diff){
        int count = 0;
        while(count<diff){
            head = head.next;
            count++;
        }
    }

    public static void main(String[] args) {
        LNode head = new LNode(4);
        head.next = new LNode(5);
        head.next.next = new LNode(2);
        head.next.next.next = new LNode(1);

        LNode head1 = new LNode(2);
        head1.next = new LNode(5);

        IntersectionOfLists intersectionOfLists = new IntersectionOfLists();
        System.out.println(intersectionOfLists.intersectionPoint(head, head1).data);
    }
}