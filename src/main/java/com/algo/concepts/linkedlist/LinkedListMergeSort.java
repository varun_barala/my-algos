package com.algo.concepts.linkedlist;


/**
 * 
 * @author ooo
 *
 */
public class LinkedListMergeSort {

    public LNode mergeSort(LNode head){
        //null or one element
        if(head == null || head.next == null){
            return head;
        }

        LNode middlePtr = middle(head);
        LNode nextToMiddlePtr = middlePtr.next;

        //divided into two parts
        middlePtr.next = null;
        LNode left = mergeSort(head);
        LNode right = mergeSort(nextToMiddlePtr);

        return mergeSortedLists(left, right);
    }

    private LNode mergeSortedLists(LNode left, LNode right){
        if(left==null){
            return right;
        }
        if(right==null){
            return left;
        }

        LNode result;
        if(left.data<=right.data){
            result = left;
            result.next = mergeSortedLists(left.next, right);
        }else{
            result = right;
            result.next = mergeSortedLists(left, right.next);
        }

        return result;
    }

    private LNode middle(LNode head){
        if(head == null){
            return head;
        }
        LNode slowPtr = head, fastPrt=head.next;
        while(fastPrt!=null && fastPrt.next!=null){
            slowPtr = slowPtr.next;
            fastPrt = fastPrt.next.next;
        }
        return slowPtr;
    }

    public static void main(String[] args) {
        //4,5,2,1,3,4,-1,3,4
        LNode head = new LNode(4);
        head.next = new LNode(5);
        head.next.next = new LNode(2);
        head.next.next.next = new LNode(1);
        head.next.next.next.next = new LNode(3);
        head.next.next.next.next.next = new LNode(4);
        head.next.next.next.next.next.next = new LNode(-1);
        head.next.next.next.next.next.next.next = new LNode(3);
        head.next.next.next.next.next.next.next.next = new LNode(4);

        LinkedListMergeSort mergeSort = new LinkedListMergeSort();
        head = mergeSort.mergeSort(head);
        LinkedListUtil.print(head);
    }

    private static LNode mergeS(LNode left, LNode right){
        if(left == null){
            return right;
        }
        if(right == null){
            return left;
        }

        LNode result;
        if(left.data<right.data){
            result = left;
            result.next = mergeS(left.next, right);
        }else{
            result = right;
            result.next = mergeS(left, right.next);
        }

        return result;
    }
}