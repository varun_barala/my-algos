package com.algo.concepts.linkedlist;

/**
 * slowPtr and fastPtr will meet
 * 
 * m + q*n + k = slowPtr
 * m + p*n + k = fastPtr
 * 
 * D(fastPtr) = 2*D(slowPtr)
 * 
 * (m + p*n + k) = 2(m + q*n + k)
 * 
 * m = (p*n-2*q*)n - k
 * 
 * @author ooo
 *
 */
public class LoopDetection {

}