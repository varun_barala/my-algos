package com.algo.concepts.linkedlist;

/**
 * 
 * @author ooo
 *
 */
class LNode {
    int data;
    LNode next;

    public LNode(int data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "[data:" + data + "]";
    }
}