package com.algo.concepts.lambba;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * 
 * @author ooo
 *
 */
public class Functions {
    public static void main(String[] args) {
        BiFunction<Integer, Integer, String> bi  = (a, b) -> "" + a + b;
        System.out.println(bi.apply(1, 1));
        System.out.println(doSomething(bi, 1, 2));

        Function<Integer, Boolean> function = (a) -> (a&(a-1))==0;
        System.out.println(function.apply(2));
        System.out.println(function.apply(3));

        Predicate<String> checkSize = (a) -> a.length()>2;
        Predicate<String> checkSize1 = (a) -> a.startsWith("a");
        System.out.println(checkSize.and(checkSize1).test("varun"));
        System.out.println(checkSize.and(checkSize1).test("arun"));
    }

    private static String doSomething(BiFunction<Integer, Integer, String> bi, int x, int y){
        return bi.apply(x, y);
    }
}