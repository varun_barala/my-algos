package com.algo.concepts.dp;

import java.util.Arrays;

public class CoinChangeProblem {

    public static void main(String[] args) {
        int n = 4;
        int[] arr = {1,2,3};
        System.out.println("total ways :: " + totalWays(arr, arr.length, n));
    }

    private static int totalWays(int[] arr, int m, int n){
        int[] table = new int[n+1];
        Arrays.fill(table, 0);

        table[0] = 1; //base case

        for(int i=0;i<m;i++){
            for(int j = arr[i]; j<=n;j++){
                table[j]+=table[j-arr[i]];
            }
        }
        return table[n];
    }
}