package com.algo.concepts.backtracking;

/**
 * 
 * @author ooo
 *
 */
public class StringPermutation {

    public static void main(String[] args) {
        String x = "abs";
        permute(x, 0, x.length()-1);
    }

    public static void permute(String str, int l, int r){
        if(l==r){
            System.out.println(str);
            return;
        }
    
        for(int i=l;i<=r;i++){
            str = swap(str, i, l);
            permute(str, l+1, r);
            str = swap(str, i, l);
        }
    }

    private static String swap(String str, int i, int j){
        char[] arr = str.toCharArray();
        char x = arr[i];
        arr[i] = arr[j];
        arr[j] = x;
        return new String(arr);
    }
}
