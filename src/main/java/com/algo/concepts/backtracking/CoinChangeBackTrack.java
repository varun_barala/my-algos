package com.algo.concepts.backtracking;

/**
 * Given a value N, if we want to make change for N cents,
 * and we have infinite supply of each of S = { S1, S2, .. , Sm} valued coins,
 * how many ways can we make the change? The order of coins doesn’t matter
 * 
 * @author ooo
 *
 */
public class CoinChangeBackTrack {
    private static int[] CACHE = {-1,-1,-1,-1, -1};
    private static int cointCount(int[] arr, int m, int n){
        if(n==0){
            CACHE[n]=0;
            return 1;
        }

        if(n<0) return 0;

        if(m<=0 && n>=1){
            return 0;
        }

        if(CACHE[n]!=-1){
            return CACHE[n];
        }

        CACHE[n] = cointCount(arr, m-1, n) + cointCount(arr, m, n-arr[m-1]);
        return CACHE[n];
    }

    public static void main(String[] args) {
        int n = 4;
        int[] arr = {1,2,3,4};
        System.out.println("total ways :: " + totalWays(arr, arr.length, n));
        System.out.println("total ways :: " + cointCount(arr, arr.length, n));
        System.out.println("min coins needed :: " + minCoinsNeeded(arr, arr.length, n, 0));
        System.out.println("min coins needed :: " + minCoin(arr, arr.length, n, 0));
    }

    /**
     * 
     * either current coin will participate or it won't participate
     * 
     * @param arr
     * @param m
     * @param n
     * @return
     */
    public static int totalWays(int[] arr, int m, int coinValue){
        if(coinValue == 0){
            return 1;
        }

        if(coinValue < 0 || m <= 0){
            return 0;
        }

        return totalWays(arr, m, coinValue-arr[m-1]) + totalWays(arr, m-1, coinValue);
    }

    public static int minCoinsNeeded(int[] arr, int m, int coinValue, int coinCount){
        if(coinValue == 0){
            return coinCount;
        }

        if(coinValue < 0 || m <= 0){
            return Integer.MAX_VALUE;
        }

        return Math.min(minCoinsNeeded(arr, m-1, coinValue-arr[m-1], coinCount+1),
                minCoinsNeeded(arr, m-1, coinValue, coinCount));
    }

    private static int minCoin(int[] arr, int m, int n, int coinCount){
        if(n==0)
            return coinCount;
        if(n<0) return Integer.MAX_VALUE;
        if(m<=0 && n>=1) return Integer.MAX_VALUE;

        return Math.min(minCoin(arr, m-1, n, coinCount), minCoin(arr, m-1, n-arr[m-1], coinCount+1));
    }
}