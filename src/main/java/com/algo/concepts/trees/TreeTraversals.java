package com.algo.concepts.trees;

import java.util.LinkedList;
import java.util.Queue;

/**
 *  Time Complexity
 *  ---------------
 *  O(n) time as all traversal algos visit every node exactly once.
 * 
 * 
 * 
 *  Space Complexity
 *  ----------------
    1) Extra Space required for Level Order Traversal is O(w) where w is maximum width of Binary Tree.
    In level order traversal, queue one by one stores nodes of different level.

    2) Extra Space required for Depth First Traversals is O(h) where h is maximum height of Binary Tree.
    In Depth First Traversals, stack (or function call stack) stores all ancestors of a node.
 * 
 * @author ooo
 *
 */
public class TreeTraversals {

    /**
     * DFS variation, first prints the left most child
     * 
     * @param root
     */
    public static void inOrder(TNode root){
        if(root == null){
            return;
        }

        inOrder(root.left);
        System.out.print(" " + root.data);
        inOrder(root.right);
    }

    /**
     * DFS variation, first prints the root
     * 
     * @param root
     */
    public static void preOrder(TNode root){
        if(root == null){
            return;
        }

        System.out.print(" " + root.data);
        preOrder(root.left);
        preOrder(root.right);
    }

    /**
     * DFS variation, first prints the children left and right
     * 
     * @param root
     */
    public static void postOrder(TNode root){
        if(root == null){
            return;
        }

        postOrder(root.left);
        postOrder(root.right);
        System.out.print(" " + root.data);
    }

    /**
     * 
     * @param root
     */
    public static void bfs(TNode root){
        if(root == null){
            return;
        }
        Queue<TNode> queue = new LinkedList<>();
        queue.add(root);

        while(!queue.isEmpty()){
            TNode tNode = queue.poll();
            System.out.print(" " + tNode.data);
            if(tNode.left != null)
                queue.add(tNode.left);
            if(tNode.right != null)
                queue.add(tNode.right);
        }
    }

    /**
     * return total nodes in the tree
     * @param root
     * @return
     */
    public static int size(TNode root){
        if(root == null){
            return 0;
        }
        return size(root.left) + 1 + size(root.right);
    }

    /**
     * binary search tree will have separate impl(more efficient)
     * 
     * @param root
     * @return
     */
    public static int findMaxInBinaryTree(TNode root){
        if(root==null){
            return Integer.MIN_VALUE;
        }

        int nodeData = root.data;
        int leftSideMax = findMaxInBinaryTree(root.left);
        int rightSideMax = findMaxInBinaryTree(root.right);

        return Math.max(nodeData, Math.max(leftSideMax, rightSideMax));
    }

    /**
     * binary search tree will have separate impl(more efficient)
     * 
     * @param root
     * @return
     */
    public static int findMinInBinaryTree(TNode root){
        if(root==null){
            return Integer.MAX_VALUE;
        }

        int nodeData = root.data;
        int leftSideMax = findMaxInBinaryTree(root.left);
        int rightSideMax = findMaxInBinaryTree(root.right);

        return Math.min(nodeData, Math.min(leftSideMax, rightSideMax));
    }

    /**
     * 
     * @param root
     */
    public static void printLeftView(TNode root){
        //TODO do level order and print first element
    }
}