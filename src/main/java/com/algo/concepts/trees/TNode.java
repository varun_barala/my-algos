package com.algo.concepts.trees;

/**
 * 
 * @author ooo
 *
 */
public class TNode {
    public int data;
    public TNode left;
    public TNode right;

    public TNode(int data) {
        this.data = data;
    }
}
