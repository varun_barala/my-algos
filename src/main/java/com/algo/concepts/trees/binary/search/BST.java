package com.algo.concepts.trees.binary.search;

import com.algo.concepts.trees.TNode;
import com.algo.concepts.trees.TreeTraversals;

/**
 * 
 * @author ooo
 *
 */
public class BST {

    /**
     * complexity is O(h) given that binary tree is balanced
     * 
     * h will be log(n) in balanced BST and will be n in skewed trees
     * 
     * @param root
     * @param data
     * @return
     */
    public TNode search(TNode root, int data){
        if(root == null || root.data == data){
            return root;
        }

        if(data>root.data){
            return search(root.right, data);
        }

        return search(root.left, data);
    }

    public void insert(TNode root, int data){
        insertRecusrion(root, data);
    }

    private TNode insertRecusrion(TNode root, int data){
        if(root == null){
            return new TNode(data);
        }

        if(data < root.data){
            root.left = insertRecusrion(root.left, data);
        }else{
            root.right = insertRecusrion(root.right, data);
        }
        return root;
    }

    /**
     * case1: If node is the leaf node
     * 
     * case2: If node has one child
     * 
     * case3: If node has two children
     * 
     * @param root
     * @param data
     */
    public void delete(TNode root, int data){
        deleteRec(root, data);
    }

    private TNode deleteRec(TNode root, int data){
        if(root == null){
            return root;
        }

        if(data > root.data){
            root.right = deleteRec(root.right, data);
        }else if(data < root.data){
            root.left = deleteRec(root.left, data);
        }else{
            /*
             * node with one child or no child
             */
            if(root.left == null){
                return root.right;
            }
            if(root.right == null){
                return root.left;
            }

            int inOrderSuccessor = minValue(root.right);
            root.data = inOrderSuccessor;
            deleteRec(root.right, inOrderSuccessor);
        }
        return root;
    }

    private int minValue(TNode root){
        int minVal = root.data;
        while(root.left!=null){
            minVal = root.left.data;
            root = root.left;
        }
        return minVal;
    }

    public static void main(String[] args) {
        BST tree = new BST();
        /* Let us create following BST 
                50 
             /     \ 
            30      70 
           /  \    /  \ 
          20   40  60   80 */
        TNode root = new TNode(50);
        tree.insert(root, 30);
        tree.insert(root, 20);
        tree.insert(root, 40);
        tree.insert(root, 70);
        tree.insert(root, 60);
        tree.insert(root, 80);

        TreeTraversals.inOrder(root);

        tree.delete(root, 20);
        System.out.println();
        TreeTraversals.inOrder(root);

        tree.delete(root, 30);
        System.out.println();
        TreeTraversals.inOrder(root);

        tree.delete(root, 50);
        System.out.println();
        TreeTraversals.inOrder(root);
    }
}