package com.algo.concepts.trees.binary;

import com.algo.concepts.trees.TNode;
import com.algo.concepts.trees.TreeTraversals;

/**
 * 
 * @author ooo
 *
 */
public class InsertionInLevelOrder {

    public TNode insertInLevelOrder(TNode root, int data){
        if(root == null){
            return new TNode(data);
        }

        insertUtil(root, data, false);
        return root;
    }

    private Metadata insertUtil(TNode root, int data, boolean isSet){
        if(root == null){
            return (!isSet)? new Metadata(new TNode(data), true) : new Metadata(null, isSet);
        }

        Metadata metadata = insertUtil(root.left, data, isSet);
        root.left = metadata.tree;
        isSet = metadata.isSet;
        metadata = insertUtil(root.right, data, isSet);
        root.right = metadata.tree;
        isSet = metadata.isSet;
        return new Metadata(root, isSet);
    }

    private static class Metadata{
        TNode tree;
        boolean isSet;

        public Metadata(TNode tnode, boolean isSet) {
            this.tree = tnode;
            this.isSet = isSet;
        }
    }

    public static void main(String[] args) {
        TNode root = new TNode(0);
        root.left = new TNode(1);
        root.right = new TNode(2);

        root.left.left = new TNode(3);
        root.right.left = new TNode(4);
        root.right.right = new TNode(5);

        TreeTraversals.bfs(root);
        System.out.println();
        TreeTraversals.preOrder(root);

        InsertionInLevelOrder insertionInLevelOrder = new InsertionInLevelOrder();
        insertionInLevelOrder.insertInLevelOrder(root, 6);

        System.out.println();
        TreeTraversals.bfs(root);
        System.out.println();
        TreeTraversals.preOrder(root);
    }
}
