package com.algo.concepts.trees.binary;

import java.util.LinkedList;
import java.util.Queue;

import com.algo.concepts.trees.TNode;
import com.algo.concepts.trees.TreeTraversals;

/**
 * Given a binary tree, delete a node from it by making sure that tree shrinks from the bottom
 * 
 * @author ooo
 *
 */
public class DeletionInBinaryTree {

    public void deletetion(TNode root, int data){
        if(root == null){
            return;
        }

        Queue<TNode> queue = new LinkedList<>();
        queue.add(root);

        TNode nodeToBeDeleted=null, nodeToBeUpdated=null;
        while(!queue.isEmpty()){
            nodeToBeDeleted = queue.poll();
            if(nodeToBeDeleted.data == data){
                nodeToBeUpdated = nodeToBeDeleted;
            }

            if(nodeToBeDeleted.left != null){
                queue.add(nodeToBeDeleted.left);
            }

            if(nodeToBeDeleted.right != null){
                queue.add(nodeToBeDeleted.right);
            }
        }

        //node not found
        if(nodeToBeUpdated==null){
            return;
        }

        int x = nodeToBeDeleted.data;
        deleteNode(root, nodeToBeDeleted);
        nodeToBeUpdated.data = x;
    }

    private void deleteNode(TNode root, TNode nodeToBeDeleted){
        Queue<TNode> queue = new LinkedList<>();
        queue.add(root);
        while(!queue.isEmpty()){
            TNode tNode = queue.poll();
            if(tNode.left == nodeToBeDeleted){
                tNode.left = null;
                return;
            }
            if(tNode.left != null){
                queue.add(tNode.left);
            }

            if(tNode.right == nodeToBeDeleted){
                tNode.right = null;
                return;
            }
            if(tNode.right != null){
                queue.add(tNode.right);
            }
        }
    }

    public static void main(String[] args) {
        TNode root = new TNode(10);
        root.left = new TNode(11);
        root.left.left = new TNode(7);
        root.left.right = new TNode(12);
        root.right = new TNode(9);
        root.right.left = new TNode(15);
        root.right.right = new TNode(8);

        //7 11 12 10 15 9 8
        TreeTraversals.inOrder(root);

        DeletionInBinaryTree deletionInBinaryTree = new DeletionInBinaryTree();
        deletionInBinaryTree.deletetion(root, 11);

        System.out.println();
        //7 8 12 10 15 9
        TreeTraversals.inOrder(root);
    }
}
