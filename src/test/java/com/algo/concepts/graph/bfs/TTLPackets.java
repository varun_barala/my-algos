package com.algo.concepts.graph.bfs;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=272
 * 
 * @author ooo
 *
 */
public class TTLPackets {
    public static void main(String[] args) {
        int size = 13;
        int[][] arr = {{10,15},{15,20},{20,25},{10,30},{30,47},
                {47,50},{25,45},{45,65},{15,35},{35,55},{20,40},
                {50,55},{35,40},{55,60}, {40,60}, {60,65}};

        List<List<Integer>> graph = new ArrayList<>(size);
        for(int i=0;i<size;i++)
            graph.add(new ArrayList<>());

        Map<Integer, Integer> map = new HashMap<>();

        int counter=0;
        for(int i=0;i<arr.length;i++){
            if(!map.containsKey(arr[i][0])){
                map.put(arr[i][0],counter++);
            }
            if(!map.containsKey(arr[i][1])){
                map.put(arr[i][1],counter++);
            }
            graph.get(map.get(arr[i][0])).add(map.get(arr[i][1]));
            graph.get(map.get(arr[i][1])).add(map.get(arr[i][0]));
        }
        int count = totalVisitedNodes(graph, map.get(35), 2);
        System.out.println(" 35,2 :: " + (size - count));

        count = totalVisitedNodes(graph, map.get(35), 3);
        System.out.println(" 35,3 :: " + (size - count));
    }

    private static int totalVisitedNodes(List<List<Integer>> graph, int src, int ttl){
        int totalCount = 1;
        BitSet visited = new BitSet(graph.size());

        ArrayDeque<Pair<Integer,Integer>> queue = new ArrayDeque<>();
        queue.add(new Pair(src, ttl));
        visited.set(src);

        while(!queue.isEmpty()){
            Pair<Integer, Integer> pair = queue.poll();
            src = pair.l;
            ttl = pair.r;
            ttl--;
            List<Integer> edges = graph.get(src);
            for(Integer edge : edges){
                if(!visited.get(edge)){
                    totalCount++;
                    visited.set(edge);
                    if(ttl>0)
                        queue.add(new Pair(edge, ttl));
                }
            }
        }
        return totalCount;
    }

    private static class Pair<L,R>{
        L l;
        R r;

        public Pair(L l, R r) {
            this.l = l;
            this.r = r;
        }
    }
}