package com.algo.concepts.graph.dfs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

/**
 * https://www.spoj.com/problems/BUGLIFE/
 * 
 * Below solution is getting TLE and I'm not in mood to optimize further
 *
 * @author ooo
 *
 */
public class BugLife {
    private static enum COLOR{
        B, //black
        W, //white
        E; //empty
    }

    private static final String CORRECT_ANS = "No suspicious bugs found!";
    private static final String WRONG_ANS = "Suspicious bugs found!";

    private static COLOR[] colors;
    private static List<List<Integer>> graph;
    private static BitSet visited;
    private static boolean STATUS=true;

    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader br = new BufferedReader(new
                InputStreamReader(System.in)); 
        int scenaiors = Integer.parseInt(br.readLine());

        for(int i=0;i<scenaiors;i++){
            String[] line = br.readLine().split(" ");
            assert line.length == 2;
            int totalBugs = Integer.parseInt(line[0]);
            int totalInteractions = Integer.parseInt(line[1]);

            graph = new ArrayList<>(totalBugs+1);
            for(int k=0;k<=totalBugs;k++){
                graph.add(new ArrayList<>());
            }
            visited = new BitSet(totalBugs+1);
            colors = new COLOR[totalBugs+1];
            Arrays.fill(colors, COLOR.E);

            for(int k=0;k<totalInteractions;k++){
                line = br.readLine().split(" ");
                int src = Integer.parseInt(line[0]);
                int dest = Integer.parseInt(line[1]);
                graph.get(src).add(dest);
                graph.get(dest).add(src);
            }

            boolean isBipartite = true;
            for(int k=1;k<=totalBugs;k++){
                if(!STATUS){
                    break;
                }
                if(!visited.get(k)){
                    colors[k] = COLOR.B;
                    dfs(k);
                }
            }
            System.out.println(String.format("Scenario #%d:", i+1));
            if(STATUS){
                System.out.println(CORRECT_ANS);
            }else{
                System.out.println(WRONG_ANS);
            }
            STATUS = true;
        }
    }

    private static void dfs(int src){
        if(!STATUS){
            return;
        }
        visited.set(src);
        List<Integer> edges = graph.get(src);
        for(Integer edge : edges){
            if(colors[src] == colors[edge]){
                STATUS = false;
                return;
            }
            colors[edge] = getComplimentColor(colors[src]);
            if(!visited.get(edge)){
                dfs(edge);
            }
        }
    }

    private static COLOR getComplimentColor(COLOR color){
        switch (color) {
        case B:
            return COLOR.W;
        case W:
            return COLOR.B;
        default:
            return COLOR.E;
        }
    }

    private static boolean isBipartite(int src){
        visited.set(src);
        for(int edge : graph.get(src)){
            if(!visited.get(edge)){
                colors[edge] = getComplimentColor(colors[src]);
                isBipartite(edge);
            }else if(colors[edge] == colors[src]){
                return false;
            }
        }
        return true;
    }
}