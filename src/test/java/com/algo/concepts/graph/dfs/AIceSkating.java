package com.algo.concepts.graph.dfs;

import java.util.*;

/**
 * 
 * http://codeforces.com/problemset/problem/217/A
 * 
 * @author ooo
 *
 */
public class AIceSkating {
    private static int[][] graph;
    private static BitSet visited;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        graph = new int[n][n];
        visited = new BitSet(n);

        int[] X = new int[n];
        int[] Y = new int[n];
        int ans=0;

        for(int i=0;i<n;i++){
            X[i] = scanner.nextInt();
            Y[i] = scanner.nextInt();
        }

        /**
         * Can be done using map
         */
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(X[i]==X[j] || Y[i]==Y[j]){
                    graph[i][j] = 1;
                }
            }
        }

        for(int i=0;i<n;i++){
            if(!visited.get(i)){
                ans++;
                dfs(i);
            }
        }
        //Let;s fine also connected component in the graph
        System.out.println(ans-1);
    }

    private static void dfs(int source){
        visited.set(source);

        for(int i=0;i<graph[source].length;i++){
            if(!visited.get(i) && graph[source][i]!=0)
                dfs(i);
        }
    }
}